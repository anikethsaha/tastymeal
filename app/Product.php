<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
	protected $fillable = ['name', 'description', 'image', 'price'];

    public function vendors(){
    	return $this->belongsToMany('App\Vendor');
    }
}
