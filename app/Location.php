<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function vendors(){
    	return $this->belongsToMany('App\Vendor');
    }
    public function city(){
    	return $this->belongsTo('App\City');
    }
}
