<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = ['name', 'phone', 'address', 'delivery_date', 'product_id', 'payment_option', 'quantity', 'order_status'];
    public function product(){
        return $this->belongsTo('App\Product');
    }
}
