<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    public function products(){
    	return $this->belongsToMany('App\Product');
    }
    public function locations(){
    	return $this->belongsToMany('App\Location');
    }
}
