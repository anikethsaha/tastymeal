<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class UserLoginController extends Controller
{
    public function __construct() {
        $this->middleware('guest');
    }

    public function showLoginForm() {
        return view('auth.login');
    }

    public function login(Request $request) {   
        $this->validate($request, [
            'mobile' => 'required|min:10',
            'password' => 'required|min:6'
        ]);

        if(Auth::attempt(['mobile' => $request->mobile, 'password' => $request->password], $request->remember)) {
            return redirect("/");
        }

        return redirect("/login");
    }
}
