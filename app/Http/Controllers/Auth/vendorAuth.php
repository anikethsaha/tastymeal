<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vendor;
use session;
use Hash;
class vendorAuth extends Controller
{
    //
    public function secr($para){
        $para=trim($para);
        $para=  strip_tags($para);
        $para=  stripslashes($para);
        $para=  htmlspecialchars($para);
    return $para;
    }
    public function Login(Request $req)
    {
        # code...
        $VendorName = $req->input('name');
        $VendorEmail = $req->input('email');
        $VendorMobile = $req->input('mobile');
        $VendorPassword = $req->input('password');
        $VendorAddress = $req->input('address');
        $VendorPassword = $this->secr($VendorPassword);
        $HashedVendorPassword = bcrypt($VendorPassword);
        $isDinner = $req->input('isDinner');
        $isLaunch = $req->input('isLaunch');
        $isBreakfast = $req->input('isBreakfast');
        //validation and sanitizaiton
        $VendorName = $this->secr($VendorName);
        $VendorEmail = $this->secr($VendorEmail);
        $VendorMobile = $this->secr($VendorMobile);
        $VendorAddress = $this->secr($VendorAddress);
        //isdinner

        if($isDinner == 'on'){
            $isDinner = true;
        }else{
            $isDinner = false;
        }
        // islaunch

        if($isLaunch == 'on'){
            $isLaunch = true;
        }else{
            $isLaunch = false;
        }
        // isBreakfast

        if($isBreakfast == 'on'){
            $isBreakfast = true;
        }else{
            $isBreakfast = false;
        }

        $VendorDBInstance = new Vendor;
        $VendorDBInstance->name = $VendorName;
        $VendorDBInstance->email = $VendorEmail;
        $VendorDBInstance->mobile = $VendorMobile;
        $VendorDBInstance->address = $VendorAddress;
        $VendorDBInstance->password = $HashedVendorPassword;
        $VendorDBInstance->rating = 0;
        $VendorDBInstance->approved = false;
        $VendorDBInstance->isDinner = $isDinner;
        $VendorDBInstance->isLunch = $isLaunch;
        $VendorDBInstance->isBreakfast = $isBreakfast;
        $VendorDBInstance->save();
        Session::flash("VendorMessage","Your Request is Submitted for approval ");
        return redirect('/');


    }

}
