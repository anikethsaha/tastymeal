<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\City;
use App\Location;
use App\Vendor;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $cities = City::all();
        return view('home')->with('cities', $cities);
    }
    public function showmenu(){
        $products = Product::all();
        return view('menu')->with('products', $products);
    }

    public function getlocations($city){
        $cit = City::where('name', $city)->get()[0];
        $locations = $cit->locations()->select('name', 'id')->get()->all();
        return response()->json($locations);
    }
    public function getvendors($id){
        $location = Location::find($id);
        $vendors = $location->vendors()->get()->all();
        return response()->json($vendors);
    }
}
