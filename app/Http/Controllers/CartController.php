<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Input;

use Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartItems = Cart::content();

        return view('cart.index')->with('cartItems', $cartItems);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cartItems = Cart::content();
        foreach ($cartItems as $cartItem) {
            $order = new Order();
            $order->name = Auth::user()->name;
            $order->mobile = Auth::user()->mobile;
            $order->address = Auth::user()->breakfast;
            $order->delivery_date = Input::get('date'.$cartItem->id);
            $order->product_id = $cartItem->id;
            $order->payment_option = 'COD';
            $order->quantity = $cartItem->qty;
            $order->order_status = 'Unconfirmed';
            $order->save();
            Cart::destroy();
        }
        return redirect('cart/thankyou');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        Cart::add($id, $product->name, 1, $product->price, ['image' => $product->image]);
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        Cart::add($id, $product->name, 1, $product->price, ['image' => $product->image ,'menu' => $request->menu]);
        return redirect('/');
    }

    public function add(){
        $cartItems = Cart::content();
        foreach ($cartItems as $cartItem) {
            $order = new Order();
            $order->name = Auth::user()->name;
            $order->mobile = Auth::user()->mobile;
            $order->address = Auth::user()->breakfast;
            $order->delivery_date = "today";
            $order->product_id = $cartItem->id;
            $order->payment_option = 'COD';
            $order->quantity = $cartItem->qty;
            $order->order_status = 'Unconfirmed';
            $order->save();
            Cart::destroy();
        }
        return redirect('cart/thankyou');
    }

    public function increase($id)
    {
        $cartItem = Cart::get($id);
        Cart::update($id, $cartItem->qty+1);
        return redirect('/cart');
    }

    public function decrease($id)
    {
        $cartItem = Cart::get($id);
        if($cartItem->qty == 1){
            Cart::remove($id);
        }
        else{
            Cart::update($id, $cartItem->qty-1);
        }
        return redirect('/cart');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function remove($id){
        Cart::remove($id);
        return redirect('/cart');
    }
}
