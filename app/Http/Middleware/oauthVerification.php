<?php

namespace App\Http\Middleware;

use Closure;

class oauthVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $fbId = env('FACEBOOK_APP_ID');
        if(!$request->cookie()->get("fbsr_'".$fbId."'") && !$request->cookie()->get('G_AUTHUSER_H') ){
            return response("NOT AUTHORIZED FOR THIS PAGE <a href='/'>home</a>")->setStatusCode(403);;
        }
        return $next($request);
    }
}
