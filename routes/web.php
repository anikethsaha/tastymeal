<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/locations/{city}', 'HomeController@getlocations');
Route::get('/vendors/{id}', 'HomeController@getvendors');
Route::get('/menu', "HomeController@showmenu");

Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/admin', 'AdminController@index');

Route::get('/login', 'Auth\UserLoginController@showLoginForm')->name('user.login');
Route::post('/login', 'Auth\UserLoginController@login')->name('user.login.submit');
Route::get('/seller',function(Request $req){
    return view('auth.seller');
});
Route::get('/register/oauth/callback/{token}/{email}/{name}','Auth\oauth@register');
Route::get('/logout', function(){
    Auth::logout();
    return redirect('/');
});

Route::get('/tiffin',function(){
    return view('tiffin');
});

Route::group(['middleware' => 'auth'], function() {
Route::get('/cart/increase/{id}','CartController@increase')->name('cart.increase');
Route::get('/cart/decrease/{id}', 'CartController@decrease')->name('cart.decrease');
Route::get('/cart/remove/{id}', 'CartController@remove')->name('cart.remove');
Route::get('/cart/add', 'CartController@add')->name('cart.add');

Route::get('/cart/thankyou', function(){
    return view('cart.thankyou');
})->name('cart.thankyou');
Route::resource('/cart', 'CartController');
});

Route::post('/register/seller','Auth\vendorAuth@Login');

Route::group(['middleware' => 'auth:admins'], function () {


    Route::get('/admin', function(){
        return view('admin.dashboard');
    });
    Route::get('admin/orderconfirm', 'OrderController@showconfirm');
    Route::get('admin/today', 'OrderController@showtoday');
    Route::get('admin/orderconfirm/{id}', 'OrderController@confirm')->name('order.confirm');
    Route::get('admin/cancel/{id}', 'OrderController@cancel')->name('order.cancel');
    Route::resource('admin/order', 'OrderController');

    Route::resource('admin/product', 'ProductController');
    Route::resource('admin/vendor', 'VendorController');

    Route::get('/admin/logout', function (){
        Auth::guard('admins')->logout();
        return redirect('/admin');
    });

    Route::get('password', 'OrderController@password');
	Route::post('password', 'OrderController@updatePassword');

	});
