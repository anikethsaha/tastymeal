<!-- @extends('layouts.front')

@section('content') -->
@include('layouts.header')
<hr><hr><hr>
<section class=" wrapper-md animated fadeInUp">
    <div class="container-fluid bg-2 aside-xxl">
        <div class="">
        <section class="login-panel ">
        <div class="vendor-login"></div>
            <header  class=" login-head">
                <h3 >Welcome to Tastymeal User Login</h3>
            </header>


            {!! Form::open(array( 'route'=> 'user.login.submit', 'class' => 'panel-body wrapper-lg')) !!}

            @if($errors)
                @foreach ($errors->all() as $message)
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> {{ $message }}
                    </div>
                @endforeach
            @endif

            <div class="">
                <label class="control-label">Mobile Number</label>
                {!! Form::text('mobile','',[ 'placeholder'=>"Enter your 10 digit mobile no.", 'class'=>""]) !!}
            </div>
            <div class="">
                <label class="control-label">Password</label>
                {!! Form::password('password',[ 'placeholder'=>"password", 'class'=>""]) !!}

            </div>
            {!! Form::submit('Sign in',[ 'class'=>""]) !!}
            {!! Form::close() !!}
            <h3 class="social-login-head">Or You Can Be Social</h3>
            <div class="g-signin2" data-onsuccess="onSignIn"></div>
            <div class="facebookBtn">
                <div class="fbBtn" onclick="loginWithFB()" >
                    <span>
                        <strong>f</strong>
                    </span>
                    <button type = "button">
                        FaceBook Login
                    </button>
                </div>
            </div>
            <div id="status">
            </div>
        </section>

    </div>

    <script>

    </script>
    </div>
</section>
@include('layouts.footer')