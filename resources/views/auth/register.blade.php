<!-- @extends('layouts.front')

@section('content') -->
@include('layouts.header')
<div class="container-fluid bg-2">
    <hr><hr><hr>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">Enter Your Mobile Number</label>

                            <div class="col-md-6">
                                <input id="mobile" type="mobile" class="form-control" name="mobile" value="{{ old('mobile') }}" required>

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Delivery Location</label>
                            <div class="col-md-6 check">
                                <div><label for="bf">Breakfast   </label><input id="bf" type="checkbox"  name="bf" value="{{ old('bf') }}" ></div>
                                <div><label for="lun">Lunch   </label><input id="lun" type="checkbox" name="lun" value="{{ old('lun') }}" ></div>
                                <div><label for="din">Dinner   </label><input id="din" type="checkbox"  name="din" value="{{ old('din') }}" ></div>
                                <div><label for="io">Instant Order   </label><input id="io" type="checkbox"  name="io" value="{{ old('io') }}" ></div>
                                <hr><hr>
                                <div id="cp"><label>Copy the below address for others</label><input id="copy" type="checkbox"></div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('bfadd') ? ' has-error' : '' }}" id="bfclass" style="display:none;">
                            <label for="bfadd" class="col-md-4 control-label">Address for Breakfast</label>

                            <div class="col-md-6">
                                <textarea id="bfadd" type="textarea" class="form-control" name="bfadd"></textarea>

                                @if ($errors->has('bfadd'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bfadd') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('lunadd') ? ' has-error' : '' }}" id="lunclass" style="display:none;">
                            <label for="lunadd" class="col-md-4 control-label">Address for Lunch</label>

                            <div class="col-md-6">
                                <textarea id="lunadd" type="textarea" class="form-control" name="lunadd"></textarea>
                                @if ($errors->has('lunadd'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lunadd') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('dinadd') ? ' has-error' : '' }}" id="dinclass" style="display:none;">
                            <label for="dinadd" class="col-md-4 control-label">Address for Dinner</label>

                            <div class="col-md-6">
                                <textarea id="dinadd" type="textarea" class="form-control" name="dinadd"  ></textarea>

                                @if ($errors->has('dinadd'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dinadd') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('ioadd') ? ' has-error' : '' }}" id="ioclass" style="display:none;">
                            <label for="ioadd" class="col-md-4 control-label">Address for Instant Order</label>

                            <div class="col-md-6">
                                <textarea id="ioadd" type="textarea" class="form-control" name="ioadd" ></textarea>

                                @if ($errors->has('ioadd'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ioadd') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')